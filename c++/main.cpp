#include <iostream>
#include <iomanip>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <xgboost/c_api.h>

#define safe_xgboost(call)                                                                          \
{                                                                                                   \
    int err = (call);                                                                               \
    if (err != 0)                                                                                   \
    {                                                                                               \
        fprintf(stderr, "%s:%d: error in %s: %s\n", __FILE__, __LINE__, #call, XGBGetLastError());  \
    exit(1);                                                                                        \
    }                                                                                               \
}


int main(int argc, char** argv)
{
    int verbosity = 0; // 0 (silent), 1 (warning), 2 (info), and 3 (debug).
    int useGPU = 0;  // set to 1 to use the GPU for training

    // load the data
    DMatrixHandle trainData, testData;
    safe_xgboost(XGDMatrixCreateFromFile("./data/agaricus.txt.train", verbosity, &trainData));
    safe_xgboost(XGDMatrixCreateFromFile("./data/agaricus.txt.test", verbosity, &testData));

    // create the booster
    BoosterHandle booster;
    DMatrixHandle evaluationDMatrixes[2] = {trainData, testData};
    safe_xgboost(XGBoosterCreate(evaluationDMatrixes, 2, &booster));

    // configure the training
    // available parameters are described here:
    //   https://xgboost.readthedocs.io/en/latest/parameter.html
    safe_xgboost(XGBoosterSetParam(booster, "tree_method", useGPU ? "gpu_hist" : "hist"));
    if (useGPU) {
        // set the GPU to use;
        // this is not necessary, but provided here as an illustration
        safe_xgboost(XGBoosterSetParam(booster, "gpu_id", "0"));
    } else {
        // avoid evaluating objective and metric on a GPU
        safe_xgboost(XGBoosterSetParam(booster, "gpu_id", "-1"));
    }

    safe_xgboost(XGBoosterSetParam(booster, "objective", "binary:hinge"));
    safe_xgboost(XGBoosterSetParam(booster, "min_child_weight", "1"));
    safe_xgboost(XGBoosterSetParam(booster, "gamma", "0.1"));
    safe_xgboost(XGBoosterSetParam(booster, "max_depth", "3"));
    safe_xgboost(XGBoosterSetParam(booster, "verbosity", verbosity ? "0" : "1"));
    safe_xgboost(XGBoosterSetParam(booster, "eval_metric", "error"));

    // train and evaluate for 10 iterations
    int numberOfTrees = 15;
    const char* evaluationNames[2] = {"train", "test"};
    const char* evaluationResult = NULL;
    for (int i = 0; i < numberOfTrees; ++i) {
        safe_xgboost(XGBoosterUpdateOneIter(booster, i, trainData));
        safe_xgboost(XGBoosterEvalOneIter(booster, i, evaluationDMatrixes, evaluationNames, 2, &evaluationResult));
        std::cout << evaluationResult << std::endl;
    }

    bst_ulong numberOfFeatures = 0;
    safe_xgboost(XGBoosterGetNumFeature(booster, &numberOfFeatures));
    std::cout << "Number of Features: " << (unsigned long) numberOfFeatures << std::endl;

    // predict
    bst_ulong outputLength = 0;
    const float* outputResult = NULL;
    int printNResults = 25;

    safe_xgboost(XGBoosterPredict(booster, testData, 0, 0, 0, &outputLength, &outputResult));
    std::cout << "y_pred: ";
    for (int i = 0; i < printNResults; ++i)
    {
        std::cout << outputResult[i] << " ";
    }
    std::cout << std::endl;

    // print true labels
    std::cout << "y_test: ";
    for (int i = 0; i < printNResults; ++i)
    {
        std::cout << outputResult[i] << " ";
    }
    std::cout << std::endl;

    {
        std::cout << "Dense Matrix Example (XGDMatrixCreateFromMat): ";

        const float values[] = {0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
        0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0,
        1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 1, 0, 0, 0, 0};

        DMatrixHandle DMatrix;
        safe_xgboost(XGDMatrixCreateFromMat(values, 1, 127, 0.0, &DMatrix));

        bst_ulong outputLength = 0;
        const float* outputResult = NULL;

        safe_xgboost(XGBoosterPredict(booster, DMatrix, 0, 0, 0, &outputLength,
            &outputResult));
        assert(outputLength == 1);

        std::cout << std::setprecision(4) << outputResult[0] << std::endl;
        safe_xgboost(XGDMatrixFree(DMatrix));
    }

    {
        std::cout << "Sparse Matrix Example (XGDMatrixCreateFromCSREx): ";

        const size_t indptr[] = {0, 22};
        const unsigned indices[] = {1, 9, 19, 21, 24, 34, 36, 39, 42, 53, 56, 65,
        69, 77, 86, 88, 92, 95, 102, 106, 117, 122};
        const float data[] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

        DMatrixHandle DMatrix;
        safe_xgboost(XGDMatrixCreateFromCSREx(indptr, indices, data, 2, 22, 127,
        &DMatrix));

        bst_ulong outputLength = 0;
        const float* outputResult = NULL;

        safe_xgboost(XGBoosterPredict(booster, DMatrix, 0, 0, 0, &outputLength,
            &outputResult));
        assert(outputLength == 1);

        std::cout << std::setprecision(4) << outputResult[0] << std::endl;
        safe_xgboost(XGDMatrixFree(DMatrix));
    }

    {
        std::cout << "Sparse Matrix Example (XGDMatrixCreateFromCSCEx): ";

        const size_t col_ptr[] = {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 7, 7, 7, 8,
        8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11, 11, 11, 11,
        11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14,
        14, 14, 14, 14, 14, 14, 15, 15, 16, 16, 16, 16, 17, 17, 17, 18, 18, 18,
        18, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20,
        20, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22};

        const unsigned indices[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0};

        const float data[] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

        DMatrixHandle DMatrix;
        safe_xgboost(XGDMatrixCreateFromCSCEx(col_ptr, indices, data, 128, 22, 1,
        &DMatrix));

        bst_ulong outputLength = 0;
        const float* outputResult = NULL;

        safe_xgboost(XGBoosterPredict(booster, DMatrix, 0, 0, 0, &outputLength,
            &outputResult));
        assert(outputLength == 1);

        std::cout << std::setprecision(4) << outputResult[0] << std::endl;
        safe_xgboost(XGDMatrixFree(DMatrix));
    }

    // free everything
    safe_xgboost(XGBoosterFree(booster));
    safe_xgboost(XGDMatrixFree(trainData));
    safe_xgboost(XGDMatrixFree(testData));
    return 0;
}